from django.db import models
from django.contrib.auth.models import User

class Constituency(models.Model):
    name = models.CharField(max_length=1000)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=1000)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name

class Area(models.Model):
    name = models.CharField(max_length=1000)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name

class Ward(models.Model):
    name = models.CharField(max_length=1000)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name

class Participant(models.Model):
    available_gender = (
        ("M", "Male"),
        ("F", "Female")
    )

    name = models.CharField(max_length=1000)
    gender = models.CharField(choices=available_gender, max_length=2)
    mobile_number = models.CharField(max_length=11, unique=True)
    email_id = models.CharField(max_length=100, blank=True, null=True)
    constituency = models.ForeignKey(Constituency, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    ward = models.ForeignKey(Ward, on_delete=models.CASCADE)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name
