from django.contrib import admin
from data_entry.models import Area, Constituency, Category, Ward, Participant
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group


class DataAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']
    ordering = ('name',)
    readonly_fields = ('added_by',)

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'added_by', None) is None:
            obj.added_by = request.user
        obj.save()

    def get_fields(self, request, obj):
        if request.user.is_superuser:
            return ['name', 'added_by']
        return ['name']
    
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(added_by__groups=request.user.groups.get())

admin.site.register(Area, DataAdmin)
admin.site.register(Constituency, DataAdmin)
admin.site.register(Category, DataAdmin)
admin.site.register(Ward, DataAdmin)


class ParticipantAdmin(DataAdmin):
    list_display = ['name', 'gender', 'mobile_number', 'email_id',
        'constituency', 'category', 'area', 'ward']
    search_fields = ['name', 'modile_number', 'email_id']
    ordering = ['name']
    filter = ['area', 'ward', 'category', 'constituency']
    readonly_fields = ('added_by',)


    def get_fields(self, request, obj):
        if request.user.is_superuser:
            return self.list_display + ['added_by']
        return self.list_display

    def get_form(self, request, obj=None, **kwargs):
        form = super(ParticipantAdmin, self).get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields['area'].queryset = Area.objects.filter(added_by__groups=request.user.groups.get())
            form.base_fields['ward'].queryset = Ward.objects.filter(added_by__groups=request.user.groups.get())
            form.base_fields['category'].queryset = Category.objects.filter(added_by__groups=request.user.groups.get())
            form.base_fields['constituency'].queryset = Constituency.objects.filter(added_by__groups=request.user.groups.get())
        return form
    
    def save_model(self, request, obj, form, change):
        if getattr(obj, 'added_by', None) is None:
            obj.added_by = request.user
        obj.save()

admin.site.register(Participant, ParticipantAdmin)

class UserAdminExt(UserAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(groups=request.user.groups.get())

    def get_form(self, request, obj=None, **kwargs):
        form = super(UserAdminExt, self).get_form(request, obj, **kwargs)
        if 'groups' in form.base_fields and not request.user.is_superuser:
            form.base_fields['groups'].queryset = Group.objects.filter(name__in=request.user.groups.all().values_list('name', flat=True))
        return form

admin.site.unregister(User)
admin.site.register(User, UserAdminExt)
